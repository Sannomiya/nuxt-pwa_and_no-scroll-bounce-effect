importScripts('/_nuxt/workbox.dev.c21f51f2.js')

const workboxSW = new self.WorkboxSW({
  "cacheId": "nuxt_pwa",
  "clientsClaim": true,
  "directoryIndex": "/"
})

workboxSW.precache([
  {
    "url": "/_nuxt/app.c7608efec4e944ebfc82.js",
    "revision": "b4c1ad3bf01aaaf2c957f1b79110991c"
  },
  {
    "url": "/_nuxt/layouts/default.b2bf3d32e27b456ece82.js",
    "revision": "414fd93dd9db7bcf3e90dc499817b39d"
  },
  {
    "url": "/_nuxt/layouts/noScroll.dee3b0473a2793e5f709.js",
    "revision": "6b95c6bfbd2ee4da2358d5c8053b23dc"
  },
  {
    "url": "/_nuxt/manifest.f2db058486280e9c26cc.js",
    "revision": "d471108ff31729befd704e8a18b55f0e"
  },
  {
    "url": "/_nuxt/pages/index.d3b3f9f60be317c2a5c7.js",
    "revision": "a50924cb1ad7b77b7ace791e8cc07f69"
  },
  {
    "url": "/_nuxt/vendor.54c15380e368d787f3f9.js",
    "revision": "26a7faedc619690491316f760c081d92"
  }
])


workboxSW.router.registerRoute(new RegExp('/_nuxt/.*'), workboxSW.strategies.cacheFirst({}), 'GET')

workboxSW.router.registerRoute(new RegExp('/.*'), workboxSW.strategies.networkFirst({}), 'GET')

