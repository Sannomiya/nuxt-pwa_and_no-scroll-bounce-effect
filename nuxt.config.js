module.exports = {
	head: {
		title: 'nuxt',
		meta: [
			{ charset: 'utf-8' },
			{ name: 'viewport', content: 'width=device-width, initial-scale=1' },
			{ hid: 'description', name: 'description', content: 'Nuxt.js project' }
		],
		link: [
			// { rel: "import", href: "~/../node_modules/@polymer/paper-button/index.html"}
		],
		script: [
			{ src: "/libs/jquery-3.3.1.min.js" },
			// { type: "module", src:"/po/paper-button.js" }
		]
	},
	loading: { color: '#3B8070' },
	build: {
		vendor: ['axios'],
		minify: {
			collapseWhitespace: false
		},
		extend (config, { isDev, isClient }) {
			if (isDev && isClient) {
				config.module.rules.push({
					enforce: 'pre',
					test: /\.(js|vue)$/,
					loader: 'eslint-loader',
					exclude: /(node_modules)/
				})
			}
		}
	},
	modules: [
		// '@nuxtjs/onesignal',
		'@nuxtjs/pwa',
	],
	// oneSignal: {
	// 	init: {
	// 		appId: '1234',
	// 		allowLocalhostAsSecureOrigin: true,
	// 		welcomeNotification: {
	// 			disable: true
	// 		}
	// 	}
	// },
	workbox:{
		dev: true,
	},
	manifest:{
		name: 'PWATest',
		short_name: 'PWA',
		title: 'PWATest',
		'og:title': 'PWATest',
		description: 'PWATest',
		'og:description': 'PWATest',
		lang: 'ja',
		theme_color: '#3B8070',
		background_color: '#3B8070',
		display: "standalone"
	},
}
